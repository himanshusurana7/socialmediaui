import { Injectable } from "@angular/core";
import { DomSanitizer } from "@angular/platform-browser";

@Injectable()
export class CommonService
{
    constructor(private sanitizer:DomSanitizer){}

    FileToByteArray(file:any)
    {
        var binaryFile : any;
        var reader = new FileReader();
        reader.readAsArrayBuffer(file);
        reader.onload = (event) => {
            if(event.target?.readyState == FileReader.DONE)
            {
                var arraybuffer = event.target.result;
                var array = new Uint8Array(arraybuffer as ArrayBuffer);
                for (var i = 0; i < array.length; i++) {
                binaryFile.push(array[i]);
                }
                console.log(binaryFile);
            }
        }
        return binaryFile;
    }

    GenerateImage(imageData:any)
    {
        return this.sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' + imageData);
    }
}