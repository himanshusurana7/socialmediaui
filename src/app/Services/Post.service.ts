import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { AuthService } from "./Auth.service";
import { CommonService } from "./Common.service";
import { UserService } from "./User.service";

@Injectable()
export class PostService
{

    public constructor(private commonService:CommonService , private auth:AuthService , private userService:UserService , private http:HttpClient){
    }
    
    private url = "https://localhost:5001/Post";
    
    async addPost(data:any)
    {
        var user = this.auth.currentUser;
        let avatar:any = "/assets/profile.png";
        let userData:any;
        await this.getPromiseUserByUsername(user.username).then(response =>{
            userData = response;
            if(userData.user['picture'] != null)
            {
                avatar = this.commonService.GenerateImage(userData.user['picture']);
            } 
        });
        
        var postData = {"username":user.username,
                        "text":data['text'],
                        "image":data['image'],
                        "postDate": new Date,
                        };
        
        let token = localStorage.getItem("token");
        let headers = new HttpHeaders().set("Authorization","Bearer "+token);
        let postResponseData : any;            
        await this.http.post(this.url,postData,{'headers':headers}).toPromise().then(response => {
            postResponseData = {"username":user.username,
            "text":data['text'],
            "image":data['image'],
            "postDate": new Date,
            "postId": response as unknown as number,
            "avatar": avatar,
            "likes":0,
            "comments": [],
            "viewComments":false
            };
        });
        
        return postResponseData;
    }

    getInitialPosts()
    {
        let token = localStorage.getItem("token");
        let headers = new HttpHeaders().set("Authorization","Bearer "+token);   
        return this.http.get(this.url + '/Initial',{'headers':headers}).toPromise();
    }

    getPostsByUsername()
    {
        let token = localStorage.getItem("token");
        let headers = new HttpHeaders().set("Authorization","Bearer "+token);   
        return this.http.get(this.url + '/'+this.auth.currentUser.username,{'headers':headers}).toPromise();   
    }

    getNextTenPosts(endingId:number)
    {
        let token = localStorage.getItem("token");
        let headers = new HttpHeaders().set("Authorization","Bearer "+token);
        
        return this.http.get(this.url + '/Ending/'+endingId,{'headers':headers}).toPromise();
    }

    like(postId:number)
    {
        var user = this.auth.currentUser;
        let likeData = {"postId":postId,"username":user.username}
        let token = localStorage.getItem("token");
        let headers = new HttpHeaders().set("Authorization","Bearer "+token);
        
        return this.http.post(this.url+"/Like",likeData,{'headers':headers});
    }

    async addComment(text:string , postId:number)
    {
        var user = this.auth.currentUser;
        let avatar:any = "/assets/profile.png";
        let data:any;
        await this.getPromiseUserByUsername(user.username).then(response =>{
            data = response;
            if(data.user['picture'] != null)
            {
                avatar = this.commonService.GenerateImage(data.user['picture']);
            } 
        });
        let commentData= {"commentId":0,"postId":postId,"username":user.username , "text":text , "commentDate": new Date}
        let commentResponseData: any;
        let token = localStorage.getItem("token");
        let headers = new HttpHeaders().set("Authorization","Bearer "+token);
        await this.http.post(this.url+"/Comment",commentData,{"headers":headers}).toPromise().then(response =>{
            
            
            commentResponseData = { "avatar":avatar,
                                    "commentId":response as unknown as number,
                                    "postId":postId,
                                    "username":user.username , 
                                    "text":text ,
                                    "commentDate": new Date
                                    }
        });
        return commentResponseData;
    }

    async getPromiseUserByUsername(username:string) : Promise<any>
    {
        return await this.userService.GetUserByUsername(username);
    }

    getComments(postId:number)
    {
        let token = localStorage.getItem("token");
        let headers = new HttpHeaders().set("Authorization","Bearer "+token);
        return this.http.get(this.url+"/Comment/"+postId,{'headers':headers});
    }
} 