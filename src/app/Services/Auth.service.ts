import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import { JwtHelperService } from "@auth0/angular-jwt";


@Injectable()
export class AuthService
{
    private Url = "https://localhost:5001";
    public constructor(private http:HttpClient){}
    Authenticate(credentials : FormGroup)
    {
        return this.http.post(this.Url + "/User/authenticate",credentials.value,{responseType:"text"});
    }

    Logout()
    {
        localStorage.removeItem("token");
    }

    IsLoggedIn()
    {
        if(localStorage.getItem("token") != null)
        {
            return new JwtHelperService().isTokenExpired();
        }
        return false;
    }

    get currentUser(){
        let token = localStorage.getItem("token");
        if(token == null)
        {
            return null;
        }
        return new JwtHelperService().decodeToken(token);
    }
}