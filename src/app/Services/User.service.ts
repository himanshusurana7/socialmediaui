import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { AuthService } from "./Auth.service";

@Injectable()
export class UserService
{
    private Url = "https://localhost:5001";
    public constructor(private http:HttpClient , private auth:AuthService){
        
    }
    AddUser(userDetails:any)
    {
        return this.http.post(this.Url+"/User" ,userDetails);
    }

    ValidateUsername(username:string)
    {
        return this.http.get(this.Url + "/User/username/" + username);
    }
    
    GetUserByUsername(username:string)
    {
        let token = localStorage.getItem("token");
        let headers = new HttpHeaders().set("Authorization","Bearer "+token);
        return this.http.get(this.Url+"/User/"+username,{'headers':headers}).toPromise();
    }

    GetAllUsers()
    {
        let token = localStorage.getItem("token");
        let headers = new HttpHeaders().set("Authorization","Bearer "+token);
        return this.http.get(this.Url+"/User",{'headers':headers}).toPromise();
    }

    UpdateUser(updatedUser:any)
    {
        let token = localStorage.getItem("token");
        let headers = new HttpHeaders().set("Authorization","Bearer "+token);
        return this.http.patch(this.Url + "/User/"+this.auth.currentUser.username,updatedUser,{'headers':headers});
    }

    ChangePassword(passwords:any)
    {
        let token = localStorage.getItem("token");
        let headers = new HttpHeaders().set("Authorization","Bearer "+token);
        return this.http.post(this.Url+'/User/password/'+this.auth.currentUser.username ,passwords,{'headers':headers});
    }
}