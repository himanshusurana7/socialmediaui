import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../Services/Auth.service';
import { CommonService } from '../Services/Common.service';
import { UserService } from '../Services/User.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
  
  data:any = [];
  show = 5;
  userProfileImage:any = "/assets/profile.png";
  username:any = "";
  window = "default";
  members:any = [];

  constructor(private commonService:CommonService , 
              private userService:UserService , 
              private authService:AuthService,
              private router:Router) { 
              }

  async ngOnInit(){
    let user = this.authService.currentUser;
    await this.userService.GetUserByUsername(user.username).then(response=>{
      this.data = response;
      if(this.data.user['picture'] == null)
      {
        this.data.user['picture'] = "/assets/profile.png";
      }
      else
      {
        this.data.user['picture'] = this.commonService.GenerateImage(this.data.user['picture']);
      }
    });

    await this.userService.GetAllUsers().then(response=>{
      this.members = response;
      for(var i=0;i<this.members.users.length;i++)
      {
        if(this.members.users[i].picture != null)
          this.members.users[i].picture = this.commonService.GenerateImage(this.members.users[i].picture);
        else
          this.members.users[i].picture = "/assets/profile.png";
      }
    });
  }

  Logout()
  {
    this.authService.Logout();
    this.router.navigate([""]);
  }

  setWindow(windowType: string)
  {
    this.window = windowType;
  }
}
