import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { CommonService } from '../Services/Common.service';
import { PostService } from '../Services/Post.service';

@Component({
  selector: 'posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

  constructor(private postService:PostService , private commonService:CommonService) { }

  commentForm = new FormGroup({
    text: new FormControl('',[Validators.required])
  });
  get text()
  {
    return this.commentForm.get("text");
  }
  postsResponse:any = [];
  isEmpty = false;
  async ngOnInit(){
    await this.postService.getPostsByUsername().then(response=>{
      this.postsResponse = response;
      if (this.postsResponse.posts.length == 0) this.isEmpty = true
      for(var i=0;i<this.postsResponse.posts.length;i++)
      {
        if(this.postsResponse.posts[i].avatar != null)
        {
          this.postsResponse.posts[i].avatar = this.commonService.GenerateImage(this.postsResponse.posts[i].avatar);
        }
        else
        {
          this.postsResponse.posts[i].avatar = "/assets/profile.png"
        }
        this.postsResponse.posts[i]["comments"] = [];
        this.postsResponse.posts[i]["viewComments"] = false; 
        if(this.postsResponse.posts[i].image != null)
          this.postsResponse.posts[i].image = this.commonService.GenerateImage(this.postsResponse.posts[i].image);
      }
    });
  }

  toggleComment(post:any)
  {
    post.viewComments = !post.viewComments;
    if(post.viewComments)
    {
      this.postService.getComments(post.postId).subscribe(response =>{
      post.comments = response;
      post.comments = post.comments.comments;
  
      for(let i=0; i<post.comments.length ; i++)
      {
        if(post.comments[i].avatar != null)
        {
          post.comments[i].avatar = this.commonService.GenerateImage(post.comments[i].avatar);
        }
        else
        {
          post.comments[i].avatar = "/assets/profile.png";
        }
      }
      });
    }
  }

  like(post:any)
  {
    this.postService.like(post.postId).subscribe(response=>{
      if(response)
      {
        post.likes += 1;
      }
      else
      {
        post.likes -= 1;
      }
    });
  }

  async addComment(form:FormGroup , post:any)
  {
    let comment:any;
    await this.postService.addComment(form.value.text , post.postId).then(response =>{
      comment =response;
    });
    (post.comments as Array<any>).push(comment);
    this.commentForm.patchValue({"text":''});
  }
}
