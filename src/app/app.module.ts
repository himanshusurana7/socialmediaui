import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations'
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { AuthService } from './Services/Auth.service';
import { UserService } from './Services/User.service';
import { SignupComponent } from './signup/signup.component';
import { ProfileComponent } from './profile/profile.component';
import { CommonService } from './Services/Common.service';
import { HomeComponent } from './home/home.component';
import { DxDrawerModule, DxListModule, DxToolbarComponent, DxToolbarModule } from 'devextreme-angular';
import { MainComponent } from './main/main.component';
import { AuthGuard } from './auth.guard';
import { PostsComponent } from './posts/posts.component';
import { DefaultComponent } from './default/default.component';
import { PostService } from './Services/Post.service';
import { ToastrModule } from 'ngx-toastr';
import { NotificationService } from './Services/Toastr.service';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SignupComponent,
    ProfileComponent,
    HomeComponent,
    MainComponent,
    PostsComponent,
    DefaultComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    RouterModule.forRoot([
      {path:"main", component: MainComponent , canActivate: [AuthGuard]},
      {path:"profile" , component:ProfileComponent , canActivate: [AuthGuard]},
      {path: "" , component:HomeComponent},
    ])
  ],
  providers: [AuthService , UserService , CommonService , AuthGuard , PostService , NotificationService],
  bootstrap: [AppComponent]
})
export class AppModule { }
