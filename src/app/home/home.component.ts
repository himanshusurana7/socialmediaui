import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../Services/Auth.service';

@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  encapsulation: ViewEncapsulation.Emulated
})
export class HomeComponent implements OnInit {

  constructor(private auth:AuthService , private router:Router) { }

  ngOnInit(): void {
    if(this.auth.IsLoggedIn())
    this.router.navigate(["/main"])
  }

  formType = "login";

  ChangeFormType(type:string)
  {
    this.formType = type;
  }
}
