import { Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../Services/Auth.service';
import { NotificationService } from '../Services/Toastr.service';
import { UserService } from '../Services/User.service';
import { DateValidator } from '../Validators/Date.validator';


@Component({
  selector: 'profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
  encapsulation:ViewEncapsulation.Emulated
})

export class ProfileComponent implements OnInit {

  @Input() userData:any;
  @Output() userDataChange = new EventEmitter();

  updateForm = new FormGroup({
    name : new FormControl('',[Validators.required]),
    email : new FormControl('',[Validators.email,Validators.required]),
    phone : new FormControl('',[Validators.maxLength(10),Validators.minLength(10),Validators.pattern("^[0-9]*$")]),
    dob : new FormControl('',[DateValidator.cannotSetToFutureDate,Validators.required]),
    picture : new FormControl(),  
    username : new FormControl('',[Validators.required])
  });

  get username()
  {
    return this.updateForm.get("username");
  }
  get name()
  {
    return this.updateForm.get("name");
  }
  get email()
  {
    return this.updateForm.get('email');
  }
  get phone()
  {
    return this.updateForm.get('phone');
  }
  get dob()
  {
    return this.updateForm.get('dob');
  }
  
  changePasswordForm = new FormGroup({
    oldPassword: new FormControl('',[Validators.required]),
    newPassword: new FormControl('',[Validators.required,Validators.minLength(8),Validators.pattern("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&+=()-])(?=\\S+$).{8,}$")]),
    reEnterPassword: new FormControl('',[Validators.required])
  });
  
  get oldPassword()
  {
    return this.changePasswordForm.get('oldPassword');
  }
  get newPassword()
  {
    return this.changePasswordForm.get('newPassword');
  }
  get reEnterPassword()
  {
    return this.changePasswordForm.get('reEnterPassword');
  }

  isUsernameAvailable:any = true;
  today = Date.now();
  imageFile :any;
  showOldPassword = false;
  showNewPassword = false;
  showReEnteredPassword = false;
  imgUrl:any;
  isPasswordMatched:any = true;
  isPasswordChanged:any = true;

  constructor(private auth:AuthService , 
              private router:Router , 
              private userService:UserService,
              private notifaction:NotificationService) { }
              
  ngOnInit(): void {
    this.imgUrl = this.userData.user['picture'];
    this.updateForm.patchValue({"username":this.userData.user['username'],
                                "email":this.userData.user['email'],
                                "phone":this.userData.user['phone'],
                                "dob":this.userData.user['dob'] == null?'':this.userData.user['dob'],
                                "name":this.userData.user['name']
                              });
  }

  ValidateUsername(e:any)
  {
    if(e.target.value)
    {
      this.userService.ValidateUsername(e.target.value).subscribe(response=>{
          this.isUsernameAvailable = response;
        });
    }
  }

  toggleOldPasswordDisplay()
  {
    this.showOldPassword = !this.showOldPassword;
  }
  toggleNewPasswordDisplay()
  {
    this.showNewPassword = !this.showNewPassword;
  }
  toggleReEnteredPasswordDisplay()
  {
    this.showReEnteredPassword = !this.showReEnteredPassword;
  }

  onUpload(event:any)
  {
    this.imageFile = event.target.files[0];
    if(event.target.files){
      var reader = new FileReader();
      reader.readAsDataURL(event.target.files[0])
      reader.onload = (event:any)=>{
        this.imgUrl = event.target.result;
      };
    }
  }

  Update(data:FormGroup)
  {
    var isUsernameChanged = false;
    var updatedUser:Array<any> = [];
    if(data.value["picture"] !=null)
      this.userData.user["picture"] = this.imgUrl;
    
    if(this.userData.user["username"] != data.value["username"])
    {
      updatedUser.push({"op":"replace" , "path":"/Username" , "value":data.value["username"]});
      this.userData.user["username"] = data.value["username"];
      isUsernameChanged = true;
    }
    if(this.userData.user["email"] != data.value["email"])
    {
      updatedUser.push({"op":"replace" , "path":"/Email" , "value":data.value["email"]});
      this.userData.user["email"] = data.value["email"];
    }
    if(this.userData.user["phone"] != data.value["phone"])
    {
      updatedUser.push({"op":"replace" , "path":"/Phone" , "value":data.value["phone"]});
      this.userData.user["phone"] = data.value["phone"];
    }
    if(this.userData.user["name"] != data.value["name"])
    {
      updatedUser.push({"op":"replace" , "path":"/Name" , "value":data.value["name"]});;
      this.userData.user["name"] = data.value["name"];
    }
    if(this.userData.user["dob"] != data.value["dob"])
    {
      updatedUser.push({"op":"replace" , "path":"/Dob" , "value":data.value["dob"]});
      this.userData.user["dob"] = data.value["dob"];
    }

    this.userDataChange.emit(this.userData);

    var binaryFile: any = [];
    if(data.value["picture"] != null)
    {
      var reader = new FileReader();
      reader.readAsArrayBuffer(this.imageFile);
      reader.onloadend = (event) => {
        if(event.target?.readyState == FileReader.DONE)
        {
          var arraybuffer = event.target.result;
          var array = new Uint8Array(arraybuffer as ArrayBuffer);
          for (var i = 0; i < array.length; i++) {
            binaryFile.push(array[i]);
          }
          data.value["picture"] = binaryFile;
          updatedUser.push({"op":"replace" , "path":"/Picture" , "value":data.value["picture"]});
          this.userService.UpdateUser(updatedUser).subscribe(resposne => {
              this.auth.Logout();
              this.notifaction.showInfo("please login to confirm the changes","Confirm Updates")
              this.router.navigate([""]);
          });
        }
      }
    }
    else
      this.userService.UpdateUser(updatedUser).subscribe(response =>{
        console.log(isUsernameChanged);
          this.auth.Logout();
          this.notifaction.showInfo("please login to confirm the changes","Confirm Updates")
          this.router.navigate([""]);
      });

  }
  
  matchPasswords(passwords:FormGroup)
  {
    if(passwords.value['newPassword'] == passwords.value['reEnterPassword'])
      this.isPasswordMatched = true;
    else
      this.isPasswordMatched = false;
  }
  changePassword(passwords:FormGroup)
  {
      this.userService.ChangePassword(passwords.value).subscribe(response=>{
        this.isPasswordChanged = response;
        if(this.isPasswordChanged)
        {
          this.auth.Logout();
          this.notifaction.showInfo("please login to confirm the changes","Confirm Updates")
          this.router.navigate([""]);
        }
      });
  }
}
