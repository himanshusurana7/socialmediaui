import { AbstractControl, ValidationErrors, ValidatorFn } from "@angular/forms";

export class DateValidator
{
    static cannotSetToFutureDate(control:AbstractControl) : ValidationErrors | null
    {
        if(control.value == null || (control.value as string) == '') return null;
        let enteredDate = (control.value as string).split("-"); 
        let today = new Date();
        if(parseInt(enteredDate[0]) < today.getFullYear()) return null;
        else if(parseInt(enteredDate[0]) > today.getFullYear()){return {'cannotSetToFutureDate':true};}
        else
        {
            if(parseInt(enteredDate[1]) < today.getMonth()+1) return null;
            else if(parseInt(enteredDate[1]) > today.getMonth()+1){return {'cannotSetToFutureDate':true};}
            else
            {
                if(parseInt(enteredDate[2]) <= today.getDate()) return null;
                else{return {'cannotSetToFutureDate':true};}
            }
        }
        
    }
}