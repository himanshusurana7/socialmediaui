import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../Services/Auth.service';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  encapsulation: ViewEncapsulation.Emulated
})
export class LoginComponent implements OnInit {

  showPassword = false;
  constructor(private authService: AuthService ,private router:Router) { }
  
  isUsernameValid:any = true;
  isPasswordValid:any = true;
  form = new FormGroup({
    username : new FormControl('',[Validators.required]),
    password : new FormControl('',[Validators.required])
  });
  ngOnInit(): void {
  }

  Login(credentials : FormGroup)
  {
    this.authService.Authenticate(credentials).subscribe(result=>{
      let response = JSON.parse(result);
      if(response.token != null)
      {
        localStorage.setItem("token",response.token);
        this.router.navigateByUrl('/main');
      }
      else
      {
        this.isUsernameValid = response.isUsernameValid;
        this.isPasswordValid = response.isPasswordValid == null? true : response.isPasswordValid;
      }
    })
  }

  togglePasswordDisplay()
  {
    this.showPassword = !this.showPassword;
  }
}
