import { HttpClient } from '@angular/common/http';
import { toBase64String } from '@angular/compiler/src/output/source_map';
import { Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CommonService } from '../Services/Common.service';
import { NotificationService } from '../Services/Toastr.service';
import { UserService } from '../Services/User.service';
import { DateValidator } from '../Validators/Date.validator';

@Component({
  selector: 'signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css'],
  encapsulation: ViewEncapsulation.Emulated
})
export class SignupComponent implements OnInit {

  @Input() formType:any;
  @Output() formTypeChange = new EventEmitter();

  showPassword = false;

  constructor(
    private http:HttpClient , 
    private service:UserService , 
    private router:Router , 
    private notification:NotificationService) {
    this.isUsernameAvailable = true;
   }

  ngOnInit(): void {
  }

  imgUrl = "/assets/profile.png" 
  form =new FormGroup({
    name : new FormControl('',[Validators.required]),
    email : new FormControl('',[Validators.email, Validators.required]),
    phone : new FormControl('',[Validators.maxLength(10),Validators.minLength(10),Validators.pattern("^[0-9]*$")]),
    dob : new FormControl('',[DateValidator.cannotSetToFutureDate,Validators.required]),
    picture : new FormControl(),  
    username : new FormControl('',[Validators.required]),
    password : new FormControl('',[Validators.required,Validators.minLength(8),Validators.pattern("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&+=()-])(?=\\S+$).{8,}$")]),
  });

  get username()
  {
    return this.form.get("username");
  }
  get name()
  {
    return this.form.get("name");
  }
  get email()
  {
    return this.form.get('email');
  }
  get phone()
  {
    return this.form.get('phone');
  }
  get password()
  {
    return this.form.get('password');
  }
  get dob()
  {
    return this.form.get('dob');
  }
  
  isUsernameAvailable:any;
  imageFile :any;
  today = Date.now();
  SignUp( userData : FormGroup)
  {
    var binaryFile: any = [];
    var data;
    if(userData.value["picture"] != null)
    {
      var reader = new FileReader();
      reader.readAsArrayBuffer(this.imageFile);
      reader.onloadend = (event) => {
        if(event.target?.readyState == FileReader.DONE)
        {
          var arraybuffer = event.target.result;
          var array = new Uint8Array(arraybuffer as ArrayBuffer);
          for (var i = 0; i < array.length; i++) {
            binaryFile.push(array[i]);
          }
          data = userData.value;
          data["picture"] = binaryFile;
          this.service.AddUser(data).subscribe(response => {
            this.formType = 'login';
            this.notification.showInfo("User signed up successfully kindly login.","Congratulations");
            this.formTypeChange.emit(this.formType);
            this.router.navigate(["/"]);
          });
        }
      }
    }
    else
    { 
      this.service.AddUser(userData.value).subscribe(response => {
        this.formType = 'login';
        this.notification.showInfo("User signed up successfully kindly login.","Congratulations");
        this.formTypeChange.emit(this.formType);
        this.router.navigate(["/"]);
      });
    }
  }

  ValidateUsername(e:any)
  {
    if(e.target.value)
    {
      this.service.ValidateUsername(e.target.value).subscribe(response=>{
          this.isUsernameAvailable = response;
        });
    }
  }

  togglePasswordDisplay()
  {
    this.showPassword = !this.showPassword;
  }

  onUpload(event:any)
  {
    this.imageFile = event.target.files[0];
    if(event.target.files){
      var reader = new FileReader();
      reader.readAsDataURL(event.target.files[0])
      reader.onload = (event:any)=>{
        this.imgUrl = event.target.result;
      };
    }
  }
}
