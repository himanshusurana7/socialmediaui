import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../Services/Auth.service';
import { CommonService } from '../Services/Common.service';
import { PostService } from '../Services/Post.service';


@Component({
  selector: 'default',
  templateUrl: './default.component.html',
  styleUrls: ['./default.component.css']
})
export class DefaultComponent implements OnInit {

  imageFile:any;
  imgUrl:any;
  imagePreview = false;
  textPreview =false;
  postPreview = false;
  username:any;
  postsResponse:any = [];
  postForm = new FormGroup({
    text: new FormControl(null),
    image: new FormControl(null)
  });
  commentForm = new FormGroup({
    text: new FormControl('',[Validators.required])
  });
  get text()
  {
    return this.postForm.get("text");
  }

  get image()
  {
    return this.postForm.get("image");
  }

  constructor(
    private postService:PostService,
    private authService:AuthService,
    private commonService:CommonService) { }

  async ngOnInit(){
    this.imagePreview = false;
    this.username = this.authService.currentUser.username;
    await this.postService.getInitialPosts().then(response =>{
      this.postsResponse = response;
      for(var i=0;i<this.postsResponse.posts.length;i++)
      {
        if(this.postsResponse.posts[i].avatar != null)
        {
          this.postsResponse.posts[i].avatar = this.commonService.GenerateImage(this.postsResponse.posts[i].avatar);
        }
        else
        {
          this.postsResponse.posts[i].avatar = "/assets/profile.png"
        }
        this.postsResponse.posts[i]["comments"] = [];
        this.postsResponse.posts[i]["viewComments"] = false; 
        if(this.postsResponse.posts[i].image != null)
          this.postsResponse.posts[i].image = this.commonService.GenerateImage(this.postsResponse.posts[i].image);
      }
    });
  }

  async loadNextPosts(postId:number)
  {
    await this.postService.getNextTenPosts(postId-1).then(response =>{
      var nextPosts:any = response;
      for(var i=0;i<nextPosts.posts.length;i++)
      {
        if(nextPosts.posts[i].avatar != null)
        {
          nextPosts.posts[i].avatar = this.commonService.GenerateImage(nextPosts.posts[i].avatar);
        }
        else
        {
          nextPosts.posts[i].avatar = "/assets/profile.png"
        }  
        nextPosts.posts[i]["comments"] = [];
        nextPosts.posts[i]["viewComments"] = false; 
        if(nextPosts.posts[i].image != null)
          nextPosts.posts[i].image = this.commonService.GenerateImage(nextPosts.posts[i].image);
        (this.postsResponse.posts as Array<any>).push(nextPosts.posts[i]); 
        }
        this.postsResponse["isLastPost"] = nextPosts.isLastPost;
    });  
  }
  toggleComment(post:any)
  {
    post.viewComments = !post.viewComments;
    if(post.viewComments)
    {
      this.postService.getComments(post.postId).subscribe(response =>{
      post.comments = response;
      post.comments = post.comments.comments;
  
      for(let i=0; i<post.comments.length ; i++)
      {
        if(post.comments[i].avatar != null)
        {
          post.comments[i].avatar = this.commonService.GenerateImage(post.comments[i].avatar);
        }
        else
        {
          post.comments[i].avatar = "/assets/profile.png";
        }
      }
      });
    }
  }

  like(post:any)
  {
    this.postService.like(post.postId).subscribe(response=>{
      if(response)
      {
        post.likes += 1;
      }
      else
      {
        post.likes -= 1;
      }
    });
  }

  CancelPost()
  {
    this.postPreview = false;
    this.postForm.patchValue({"text":null});
    this.textPreview = false;
    this.postForm.patchValue({"image":null});
    this.imgUrl = null;
    this.imagePreview = false;
    this.imageFile = null;
  }

  async Post(post:FormGroup)
  {
    if(this.PostValidation(post))
    {
      var binaryFile: any = [];
      var data;
      if(post.value["image"] == null || post.value["image"] == "")
      {
        await this.postService.addPost(post.value).then(response =>{ 
          (this.postsResponse.posts as Array<any>).unshift(response);
          this.postForm.patchValue({"text":''});
        }); 
      }
      else
      {
        var reader = new FileReader();
        reader.readAsArrayBuffer(this.imageFile);
        reader.onloadend = async (event) => {
          if(event.target?.readyState == FileReader.DONE)
          {
            var arraybuffer = event.target.result;
            var array = new Uint8Array(arraybuffer as ArrayBuffer);
            for (var i = 0; i < array.length; i++) {
              binaryFile.push(array[i]);
            }
            data = post.value;
            data["image"] = binaryFile;
            await this.postService.addPost(data).then(response=>{
              response['image'] = this.imgUrl; 
              (this.postsResponse.posts as Array<any>).unshift(response);
              this.postForm.patchValue({"image":''});
            });
          }
          if(post.value["text"] != null || post.value["text"] != '')
          {
            this.postForm.patchValue({"text":''});
          }
        }
      } 
    }
    this.postPreview = false;
    this.imagePreview = false;
    this.textPreview = false;
  }

  PostValidation(form:FormGroup)
  {
    console.log(form.value['text'].length);
    if(form.value['text'] != null)
    {
      if(form.value['text'].length > 0)
      {
        form.value['text'] = (form.value['text'] as string).trimStart();
        form.value['text'] = (form.value['text'] as string).trimEnd();
      }
    }
    
    console.log(form.value);
    if((form.value.text == "" || form.value.text == null) && form.value.image == null)
    {
      return false;
    }
    return true;
  }

  async addComment(form:FormGroup , post:any)
  {
    let comment:any;
    await this.postService.addComment(form.value.text , post.postId).then(response =>{
      comment =response;
    });
    (post.comments as Array<any>).push(comment);
    this.commentForm.patchValue({"text":''});
  }

  textUpload()
  {
    this.postPreview = true;
    this.textPreview = true;
  }

  onUpload(event:any)
  {
    this.imageFile = event.target.files[0];
    if(event.target.files){
      var reader = new FileReader();
      reader.readAsDataURL(event.target.files[0])
      reader.onload = (event:any)=>{
        this.imgUrl = event.target.result;
      };
      this.postPreview =true;
      this.imagePreview = true;
    }
  }
}
